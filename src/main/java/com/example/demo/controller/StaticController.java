package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.util.ResourceUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.*;

/**
 * LetItGo
 *
 * @author zzc
 * @version 1.00
 * @Date 2019/6/3-15:12
 */
@Controller
@CrossOrigin
public class StaticController {

    @RequestMapping(value = "/")
    public String index(){
        return "largePointsEcharts";
    }

    /**
     * readFileToString
     * 读取文件内容 返回json String
     * @return java.lang.String
     * @author zzc
     * @Date 2019/6/4-9:19
     * @version 1.00
     */
    public static String readFileToString(File file) throws IOException {
        FileInputStream fis = new FileInputStream(file);
        InputStreamReader in = new InputStreamReader(fis);
        BufferedReader reader = new BufferedReader(in);
        String line;
        StringBuffer buffer = new StringBuffer();
        line = reader.readLine();
        while (line != null) {
            buffer.append(line);
            buffer.append("\n");
            line = reader.readLine();
        }
        fis.close();
        in.close();
        reader.close();
        return buffer.toString();
    }

    @RequestMapping(value = "/getALLPoints")
    @ResponseBody
    public String getALLPoints(String name) throws IOException {
        System.out.println(name);
        String jsonString="";
        if (StringUtils.isEmpty(name)){
            File file = ResourceUtils.getFile("classpath:static/parking.json");
            return readFileToString(file);
        }
        if ("metro".equals(name)){
            File file = ResourceUtils.getFile("classpath:static/metro.json");
            return readFileToString(file);
        }
        if ("elevator".equals(name)){
            File file = ResourceUtils.getFile("classpath:static/elevator.json");
            return readFileToString(file);
        }
        if ("led".equals(name)){
            File file = ResourceUtils.getFile("classpath:static/led.json");
            return readFileToString(file);
        }
        return jsonString;
    }

}
